# Spatial Analysis of Histology in 3D

Supplementary material for article Ruusuvuori et al. "Spatial Analysis of Histology in 3D: Quantification and Visualization of Organ and Tumor Level Tissue Environment"

https://www.dropbox.com/sh/3cr4icsvxsqokp7/AACGuYgIIETNz6fLhR2t_3ywa?dl=0